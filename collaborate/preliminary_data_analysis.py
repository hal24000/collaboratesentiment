
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from plotly.subplots import make_subplots

class CollaboratePreliminaryAnalysis():
    
    def get_percentage_change_cols(self, df, cols_parent = None):
        """Takes a list of data frame columns and plots the percetage change"""
        if not cols_parent:
            cols_parent = list(df)
            print(cols_parent)
        else:
            cols_parent = cols_parent 
        for i in cols_parent:
            df[f"percentage_change_{i}"] = df[i].pct_change()
        return df 
    
    


    
    
    def plot_telemetry_df(self, df, time_col, title = None):
        """Plots a telemenetry based dataFrame"""
        fig = px.line(df, 
                      x=time_col, 
                      y=df.columns,
                      title=title)
        fig.update_xaxes(
        dtick="M1",
        tickformat="%b\n%Y")
        return fig
    
    def get_xaxis_title(self, column_header):
        """gets the title for x axis"""
        if "debiet" in column_header:
            return "Flow Rate (m3/hr)"
        elif "Chloor" in column_header:
            return "Chlorine (mg/L)"
        elif "pH" in column_header:
            return "pH"
        else:
            return "Axis label couldnt be infered from column header"
    

    def get_stacked_scatter(self, df, xcol, exclude_cols = None):
        """gets a stacked scatter for df"""
        if exclude_cols:
            df = df[[i for i in list(df) if i not in exclude_cols]]
        else:
            df = df 
        nrows = len(list(df)) - 1

        print("n rows ", nrows)

        subplot_columns = tuple([i for i in list(df) if i not in [xcol]])

        fig = make_subplots(rows= nrows,
                            cols=1, 
                            shared_xaxes=True,
                            x_title= 'Date',
                            subplot_titles= subplot_columns )

        x = df[xcol]
        row = 0
        for i in subplot_columns:
            print(i)
            row += 1
            fig.append_trace(go.Scatter(
                x= x, # datetime 
                y= df[i].values,  #dfmeasures['rainfall'].values,
                name = i, 

                ), row= row , col=1)
            fig.update_yaxes(title_text= self.get_xaxis_title(column_header = i), row=row, col=1)

        fig.update_layout(
            #xaxis = {'rangeslider' : {'visible' : True}}
        )

        #fig['layout']['sliders'][0]['pad']=dict(r= 10, t= 150,)

        return fig 
    
    def get_boxplots(self, df, jitter = 0.3, pointposition = -1.8, boxpoints = 'all'):

        """For a site gets the hsitograms for each feature """

        fig = make_subplots(rows=3, 
                            cols=3, 
                            start_cell="top-left", 
                            subplot_titles=('DetectorB1',
                                             'DetectorB2',
                                             'DetectorB3',
                                             'F24Response',
                                             'Response',
                                             'SignalHealth',
                                             'Threshold1',
                                             'Threshold2',
                                             'temperature'), 
                           )


        fig.add_trace(go.Box( 
                                y=df['DetectorB1'], 
                                boxmean='sd', 
                                name= 'DetectorB1', 
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ), 
                                row=1, 
                                col=1, 
                             )

        fig.add_trace(go.Box( 
                                y=df['DetectorB2'], 
                                boxmean='sd',
                                name= 'DetectorB2', 
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=1, 
                                col=2, 
                             )

        fig.add_trace(go.Box( 
                                y=df['DetectorB3'], 
                                boxmean='sd', 
                                name= 'DetectorB3',
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=1, 
                                col=3, 
                             )

        fig.add_trace(go.Box( 
                                y=df['F24Response'], 
                                boxmean='sd',
                                name= 'F24Response',
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=2, 
                                col=1, 
                             )

        fig.add_trace(go.Box(
                                y=df['Response'],
                                boxmean='sd',
                                name= 'Response',
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=2, 
                                col=2, 
                             )

        fig.add_trace(go.Box(
                                y=df['SignalHealth'], 
                                boxmean='sd',
                                name= 'SignalHealth',
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=2, 
                                col=3, 
                             )

        fig.add_trace(go.Box( 
                                y=df['Threshold1'], 
                                boxmean='sd',
                                name= 'Threshold1',
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=3, 
                                col=1, 
                             )
        fig.add_trace(go.Box( 
                                y=df['Threshold2'], 
                                boxmean='sd',
                                name= 'Threshold2',
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=3, 
                                col=2, 
                             )
        fig.add_trace(go.Box( 
                                y=df['temperature'], 
                                boxmean='sd',
                                name='temperature' ,
                                boxpoints=boxpoints, 
                                jitter=jitter, 
                                pointpos=pointposition, 
                                ),
                                row=3, 
                                col=3, 
                             )

        return fig

    def get_histograms(self, df, histnorm='probability'):

        """For a site gets the hsitograms for each feature """

        fig = make_subplots(rows=3, 
                            cols=3, 
                            start_cell="top-left", 
                            subplot_titles=('DetectorB1',
                                             'DetectorB2',
                                             'DetectorB3',
                                             'F24Response',
                                             'Response',
                                             'SignalHealth',
                                             'Threshold1',
                                             'Threshold2',
                                             'temperature'), 
                            )


        fig.add_trace(go.Histogram( 
                                x=df['DetectorB1'],
                                name= 'DetectorB1',
                                histnorm= histnorm, 
                                ), 
                                row=1, 
                                col=1, 
                             )

        fig.add_trace(go.Histogram( 
                                x=df['DetectorB2'], 
                                histnorm= histnorm, 
                                name = 'DetectorB2',
                                ),
                                row=1, 
                                col=2, 
                             )

        fig.add_trace(go.Histogram( 
                                x=df['DetectorB3'], 
                                histnorm=histnorm,
                                name = 'DetectorB3',
                                ),
                                row=1, 
                                col=3, 
                             )

        fig.add_trace(go.Histogram( 
                                x=df['F24Response'], 
                                histnorm=histnorm,
                                name = 'F24Response',
                                ),
                                row=2, 
                                col=1, 
                             )

        fig.add_trace(go.Histogram(
                                x=df['Response'],
                                histnorm=histnorm,
                                name = 'Response',
                                ),
                                row=2, 
                                col=2, 
                             )

        fig.add_trace(go.Histogram(
                                x=df['SignalHealth'], 
                                histnorm=histnorm,
                                name = 'SignalHealth',
                                ),
                                row=2, 
                                col=3, 
                             )

        fig.add_trace(go.Histogram( 
                                x=df['Threshold1'], 
                                histnorm=histnorm,
                                name = 'Threshold1',
                                ),
                                row=3, 
                                col=1, 
                             )
        fig.add_trace(go.Histogram( 
                                x=df['Threshold2'], 
                                histnorm=histnorm,
                                name = 'Threshold2', 
                                ),
                                row=3, 
                                col=2, 
                             )
        fig.add_trace(go.Histogram( 
                                x=df['temperature'], 
                                histnorm=histnorm,
                                name = 'temperature',
                                ),
                                row=3, 
                                col=3, 
                             )

        return fig
    
    
    
    def get_sactterplots(self, df):

        """For a site gets the hsitograms for each feature """

        fig = make_subplots(rows=3, 
                            cols=3, 
                            start_cell="top-left", 
                            subplot_titles=('DetectorB1',
                                             'DetectorB2',
                                             'DetectorB3',
                                             'F24Response',
                                             'Response',
                                             'SignalHealth',
                                             'Threshold1',
                                             'Threshold2',
                                             'temperature'), 
                            )


        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['DetectorB1'], 
                                name= 'DetectorB1',
                                ),
                                row=1, 
                                col=1, 
                               
                             )

        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['DetectorB2'], 
                                name= 'DetectorB2',
                                ),
                                row=1, 
                                col=2, 
                             )

        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['DetectorB3'], 
                                name= 'DetectorB3',
                                ),
                                row=1, 
                                col=3, 
                             )

        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['F24Response'], 
                                name= 'F24Response',
                                ),
                                row=2, 
                                col=1, 
                             )

        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['Response'], 
                                name= 'Response',
                                ),
                                row=2, 
                                col=2, 
                             )

        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['SignalHealth'], 
                                name= 'SignalHealth',
                                ),
                                row=2, 
                                col=3, 
                             )

        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['Threshold1'], 
                                name= 'Threshold1',
                                ),
                                row=3, 
                                col=1, 
                             )
        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['Threshold2'], 
                                name= 'Threshold2',
                                ),
                                row=3, 
                                col=2, 
                             )
        fig.add_trace(go.Scatter(
                                x=df.index, 
                                y=df['temperature'], 
                                name= 'temperature',
                                ),
                                row=3, 
                                col=3, 
                             )

        return fig