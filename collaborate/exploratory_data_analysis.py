import scipy

import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler

class CollaboarteEDA():
    """A class holding methods for EDA on dataframe defined at instantation"""

    #@property
    #def _constructor(self):
    #    return SubclassedDataFrame


    def __init__(self, df):
        self.df = df
    
    

    def get_inter_quartile_range_data(self, upperq = 0.75, lowerq = 0.25):
        
        dict_iqrs = {}
        lower_outliers = {}
        lower_outliers_count = {}
        lower_outliers_per = {}
        upper_outliers = {}
        upper_outliers_count = {}
        upper_outliers_per = {}
        
        for col in list(self.df):
            
            if self.df[col].dtype != np.dtype('object'):
                #print(f"column {col} is not an object calculating IQR")
                q1 = self.df[col].quantile(lowerq)
                #print(f"q1 {col} is {q1}")
                q3 = self.df[col].quantile(upperq)
                #print(f"q3 {col} is {q3}")
                iqr = q3 - q1
                dict_iqrs[col] = iqr
                
                lower_outlier_values = self.df[col].loc[self.df[col] < (q1 - (iqr *1.5)) ].values
                lower_outliers[col] = lower_outlier_values
                lower_outliers_count[col] = len(lower_outlier_values)
                lower_outliers_per[col] = (len(lower_outlier_values)/len(self.df))*100
                
                upper_outlier_values = self.df[col].loc[self.df[col] > (q3 + (iqr *1.5))].values
                upper_outliers[col] = upper_outlier_values
                upper_outliers_count[col] = len(upper_outlier_values)
                upper_outliers_per[col] = (len(upper_outlier_values)/len(self.df))*100
                
                
            else:
                print(col, "is an object skiped")
                
        df_result = pd.DataFrame.from_dict(dict_iqrs, orient = 'index').T
        df_result.rename({0: "IQR"}, inplace = True)
        
        df_lower_outiler_count = pd.DataFrame.from_dict(lower_outliers_count, orient = 'index').T
        df_lower_outiler_count.rename({0: "Suspected Lower Outlier Count"}, inplace = True)
        df_lower_outiler_per = pd.DataFrame.from_dict(lower_outliers_per, orient = 'index').T
        df_lower_outiler_per.rename({0: "Suspected Lower Outlier %"}, inplace = True)
        
        df_upper_outiler_count = pd.DataFrame.from_dict(upper_outliers_count, orient = 'index').T
        df_upper_outiler_count.rename({0: "Suspected Upper Outlier Count"}, inplace = True)
        df_upper_outiler_per = pd.DataFrame.from_dict(upper_outliers_per, orient = 'index').T
        df_upper_outiler_per.rename({0: "Suspected Upper Outlier %"}, inplace = True)
        
        df_result = df_result.append(df_lower_outiler_count)
        df_result = df_result.append(df_lower_outiler_per)
        
        df_result = df_result.append(df_upper_outiler_count)
        df_result = df_result.append(df_upper_outiler_per)
        
    
        return df_result #, df_lower_outiler_count, upper_outliers_count

    def get_box_plot_values(self, column, unit):
        """for a given dataframe column (or subset of column) get the boxplot values for EDA
        could be used to test new fucntion 

        """
        df_ = self.df[column]


        count = df_.count()
        mean = df_.mean()
        std_dev = df_.std()

        variance = df_.var()

        mode = df_.mode()
        mode_count = len(df_.loc[ df_ == df_.mode().values[0]])
        median = df_.median()

        dict_general_stats = {'count' : count, 
                             'mean' : mean, 
                             'std_dev' : std_dev, 
                            'mode' : mode, 
                              'mode_count' : mode_count, 
                              'median' : median, 
                              'unit' : unit, 

                             }

        bp_data = pd.DataFrame.boxplot(df_,  return_type='dict')
        outliers = [flier.get_ydata() for flier in bp_data["fliers"]]
        boxes = [box.get_ydata() for box in bp_data["boxes"]]
        medians = [median.get_ydata() for median in bp_data["medians"]]
        whiskers = [whiskers.get_ydata() for whiskers in bp_data["whiskers"]]

        q0 = whiskers[0][1]
        q1 = whiskers[0][0]
        q3 = whiskers[1][0]
        q4 = whiskers[1][1]

        # get data on outliers 
        lower_outliers = [i for i in outliers[0] if i < q1]
        upper_outliers = [i for i in outliers[0] if i > q3]
        total_ouliers = len(lower_outliers) + len(upper_outliers)

        dict_outliers = { 'total outliers' :  total_ouliers, 
                        'outliers_percent' : total_ouliers/count *100, 

                        'lower_outliers' :  {'values' :lower_outliers, 
                         'num_lower_outliers' : len(lower_outliers), 
                        'min_lower_outlier' : min(lower_outliers, default=0), 
                        'max_lower_outlier' : max(lower_outliers, default=0), 
                                              }, 

                        'upper_outliers' :  {'values' : upper_outliers, 
                        'num_upper_outliers' : len(upper_outliers), 
                        'min_upper_outlier' : min(upper_outliers, default=0), 
                        'max_upper_outlier' : max(upper_outliers, default=0), 
                                              }
                           }

        # get median 
        # do we need to check medians are equal why are their 2 medians? 
        if medians[0][0] == medians[0][1]:
            print("matching of medians retunred")
        else:
            print ("mediand not matching, invetsigate: ", medians)

        median = medians[0][0]

        dict_quartiles = { 'q0' : q0, 
                          'q1' : q1, 
                          'median' : median, 
                          'q3' : q3, 
                          'q4' : q4, 
                          'IQR' : q3 - q1
                         }

        dict_boxplot_values = { 'dict_general_stats' : dict_general_stats, 
                                'dict_outliers' : dict_outliers, 
                               'dict_quartiles' : dict_quartiles

                              }
        return dict_boxplot_values 
    

    def get_enriched_df_describe(self ):
        """Enriches pandas describe with additional stat measure built into pandas"""
        original_cols = list(self.df)
        self.df = self.df.select_dtypes(include=np.number)
        numeric_cols = list(self.df)
        print(f"object columns dropeed: {list(set(original_cols) ^ set(numeric_cols))}")
        df_enriched = self.df.describe().append(pd.DataFrame(data = [
                         [i for i in self.df.median() ], 
                         [i for i in self.df.mode().values.tolist()[0]  if not isinstance(i, str)], 
                         [i for i in self.df.var()],
                         [i for i in self.df.kurtosis()], 
                         [i for i in self.df.skew()], 
                        ], 
                 index = ["median", "mode", "variance" , "kurtosis", "skew"], 
                 columns = [i for i in list(self.df) if i not in ["Sensor", "site"]]
                ))
        return df_enriched
    
    def get_full_stats_dataframe(self):
        """gets a stats dataframe using methods in the class"""
        df_stats = self.get_enriched_df_describe()
        df_stats = df_stats.append(self.get_inter_quartile_range_data())
        return df_stats


