
import os
import datetime as dt
import json

class SaveToFile():

    def __init__(self, tweets=None, filename=None):
        pass
        #self.tweets = tweets
        #self.filename = filename
        #self.max_id = max_id

    def open_file():
        pass

    def write_tweets(self, tweets, filename):
        ''' Function that appends tweets to a file. '''

        with open(filename, 'a') as f:
            for tweet in tweets:
                json.dump(tweet._json, f)
                f.write('\n')
        return print("written to file")
