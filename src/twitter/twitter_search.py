import tweepy
import os
import json
import datetime as dt
import sys
import time

from twitter_save import SaveToFile
from twitter_settings import Settings


class TweetSearch:

        def __init__(self, api,):
                self.api = api


        def tweet_search(self, max_id, max_tweets, query,
                        since_id=None, 
                        #geocode = search_settings.UK
                        ): #removed geocode
        #pi, search_phrase, max_tweets, max_id=max_id, since_id=since_id,
            ''' Function that takes in a search string 'query', the maximum
                number of tweets 'max_tweets', and the minimum (i.e., starting)
                tweet id. It returns a list of tweepy.models.Status objects. '''

            searched_tweets = []
            print("Max_id", max_id)
            while len(searched_tweets) < max_tweets:
                remaining_tweets = max_tweets - len(searched_tweets)
                try:
                    new_tweets = self.api.search_tweets(q=query, 
                                            count=remaining_tweets,
                                            since_id=str(since_id),
        				                    max_id=str(max_id-1),
        				                    tweet_mode="extended", 
                                            #truncated = False, 
                                            #geocode= search_settings.UK,
                                            )
                    print('found',len(new_tweets),'tweets')
                    if not new_tweets:
                        print('no tweets found')
                        break
                    searched_tweets.extend(new_tweets)
                    max_id = new_tweets[-1].id
                except tweepy.errors.TweepyException: #tweepy.TweepError:
                    print('exception raised, waiting 15 minutes')
                    print('(until:', dt.datetime.now()+dt.timedelta(minutes=15), ')')
                    time.sleep(15*60)
                    break # stop the loop
            return (searched_tweets, max_id)


        def get_tweet_id(self, date='', days_ago=9, query='a'):
            ''' Function that gets the ID of a tweet. This ID can then be
                used as a 'starting point' from which to search. The query is
                required and has been set to a commonly used word by default.
                The variable 'days_ago' has been initialized to the maximum
                amount we are able to search back in time (9).'''

            if date:
                # return an ID from the start of the given day
                td = date + dt.timedelta(days=1)
                tweet_date = '{0}-{1:0>2}-{2:0>2}'.format(td.year, td.month, td.day)
                tweet = api.search(q=query, count=1, until=tweet_date, 
        				                    tweet_mode="extended", 
                                            #truncated = False, 
                                             )
            else:
                # return an ID from __ days ago
                td = dt.datetime.now() - dt.timedelta(days=days_ago)
                tweet_date = '{0}-{1:0>2}-{2:0>2}'.format(td.year, td.month, td.day)
                # get list of up to 10 tweets
                tweet = self.api.search_tweets(q=query, count=10, until=tweet_date, 
        				                    tweet_mode="extended",   
                                            #truncated = False,
                                              )
                print('search limit (start/stop):',tweet[0].created_at)
                # return the id of the first tweet in the list
                return tweet[0].id

        def write_tweets(self, tweets, filename):
            ''' Function that appends tweets to a file. '''
    

            with open(filename, 'a') as f:
                for tweet in tweets:
                    json.dump(tweet._json, f)
                    f.write('\n')
            return print("written to file")

        def search_loop (self,
                        search_phrases,
                        ):
                """Loops through serch phrases and saves tweets to file"""
                tweet_save = SaveToFile()
                search_settings = Settings()

                for search_phrase in search_phrases:

                    print('Search phrase =', search_phrase)

                    ''' other variables '''
                    name = search_phrase
                    print(name)
                    json_file_root = name + '/'  + name
                    os.makedirs(os.path.dirname(json_file_root), exist_ok=True)
                    read_IDs = False

                    # open a file in which to store the tweets
                    if search_settings.max_days_old - search_settings.min_days_old == 1:
                        d = dt.datetime.now() - dt.timedelta(days= search_settings.min_days_old)
                        day = '{0}-{1:0>2}-{2:0>2}'.format(d.year, d.month, d.day)
                    else:
                        d1 = dt.datetime.now() - dt.timedelta(days=search_settings.max_days_old-1)
                        d2 = dt.datetime.now() - dt.timedelta(days=search_settings.min_days_old)
                        day = '{0}-{1:0>2}-{2:0>2}_to_{3}-{4:0>2}-{5:0>2}'.format(
                              d1.year, d1.month, d1.day, d2.year, d2.month, d2.day)
                    json_file = json_file_root + '_' + day + '.json'
                    if os.path.isfile(json_file):
                        print('Appending tweets to file named: ',json_file)
                        read_IDs = True

                    # authorize and load the twitter API
                    #api = load_api()





                    # set the 'starting point' ID for tweet collection
                    if read_IDs:
                        # open the json file and get the latest tweet ID
                        with open(json_file, 'r') as f:
                            try:
                                lines = f.readlines()
                                max_id = json.loads(lines[-1])['id']
                                print('Searching from the bottom ID in file')
                            except Exception as e:
                                print("ERRORR", e)
                    else:
                        # get the ID of a tweet that is min_days_old
                        if search_settings.min_days_old == 0:
                            max_id = -1
                        else:
                            max_id = self.get_tweet_id(days_ago=(search_settings.min_days_old-1))
                    # set the smallest ID to search for
                    since_id = self.get_tweet_id(days_ago=(search_settings.max_days_old-1))
                    print('max id (starting point) =', max_id)
                    print('since id (ending point) =', since_id)



                    ''' tweet gathering loop  '''
                    start = dt.datetime.now()
                    end = start + dt.timedelta(hours=search_settings.time_limit)
                    count, exitcount = 0, 0
                    while dt.datetime.now() < end:
                        count += 1
                        print('count =',count)
                        # collect tweets and update max_id

                        tweets, max_id = self.tweet_search(query=search_phrase,
                                                      max_tweets=search_settings.max_tweets,
                                                      max_id=max_id,
                                                      since_id=since_id,)
                                                      #geocode=search_settings.UK)

                        # write tweets to file in JSON format
                        if tweets:
                            self.write_tweets(tweets, json_file)
                            print ("tweets written to file")
                            exitcount = 0
                        else:
                            exitcount += 1
                            if exitcount == 3:
                                if search_phrase == search_phrases[-1]:
                                    sys.exit('Maximum number of empty tweet strings reached - exiting')
                                else:
                                    print('Maximum number of empty tweet strings reached - breaking')
                                    break
