#Open in Python Editor
import tweepy
from tweepy import OAuthHandler

import datetime as dt


from twitter_auth import AuthTwitter
from twitter_search import TweetSearch



''' search variables: '''


water_companies = ['Anglian Water', 
                'Dŵr Cymru',
                'Welsh Water', 
                'Northumbrian Water',
                'Severn Trent Water', 
                'Southern Water', 
                'South West Water',
                'Thames Water', 
                'United Utilities', 
                'Wessex Water',
                'Yorkshire Water', 
                'Hafren Dyfrdwy', 
                'Affinity Water', 
                'Albion Water', 
                'Bournemouth Water',
                'Bristol Water', 
               'Cambridge Water Company',
               'Cholderton and District Water Company', 
               'Essex and Suffolk Water',
               'Hartlepool Water', 
               'Portsmouth Water', 
               'South East Water',
               'South Staffordshire Water', 
               'Sutton and East Surrey Water',
               'Youlgrave Waterworks',
                  ]

water_companies_twitter_handle = [ '@Bristolwater']

key_water_words = ['clean', 
                   'waste', 
                   'sewage', 
                   'dirty', 
                   'tap', 
                   'drain', 
                   'drain blocked', 
                   'overflow', 
                   'flouride', 
                  ]

key_water_combined = [ i + " " + j for i in water_companies for j in key_water_words ]


# combines all above 
search_phrases = [ ]
search_phrases.extend(water_companies ) 
search_phrases.extend(water_companies_twitter_handle ) 
search_phrases.extend(key_water_combined ) 





def main():
    ''' This is a script that continuously searches for tweets
        that were created over a given number of days. The search
        dates and search phrase can be changed below. '''

    test = AuthTwitter() #instaniate authorisation class
    api = test.load_api() #load_api() of class method
    tweet_class = TweetSearch(api) # instantiante tweet search class
    tweet_class.search_loop(search_phrases)



if __name__ == "__main__":
    main()
